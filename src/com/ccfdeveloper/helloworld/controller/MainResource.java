package com.ccfdeveloper.helloworld.controller;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Classe de REST Jersey default do Plugin.
 * 
 * @author Cristiano Farias <cristiano.farias@multi24h.com.br>
 * @since 11 de abr de 2019
 */
@Path("/root")
public class MainResource {

	/**
	 * Metodo para verificaçaõ.
	 * 
	 * @return Hora atual no host do plugin.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes("text/json; charset=UTF-8")
	@Path("now")
	public Response getDateTime() {
		return Response.status(200).entity(new Date().toString()).build();
	}

	/**
	 * Metodo para verificaçaõ.
	 * 
	 * @return Hora atual no host do plugin.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("text/json; charset=UTF-8")
	@Path("now/json")
	public Response getDateTimeJson() {
		return Response.status(200).entity(new Date().toString()).build();
	}

}